﻿using UnityEngine;
using UnityEngine.UI;
using Mirror;
using System;

namespace Complete
{
    public class TankHealth : NetworkBehaviour
    {
        private GameManager gameManager => GameManager.Instance;
        public const float m_StartingHealth = 100f;               // The amount of health each tank starts with
        public Slider m_Slider;                             // The slider to represent how much health the tank currently has
        public Image m_FillImage;                           // The image component of the slider
        public Color m_FullHealthColor = Color.green;       // The color the health bar will be when on full health
        public Color m_ZeroHealthColor = Color.red;         // The color the health bar will be when on no health
        public GameObject m_ExplosionPrefab;                // A prefab that will be instantiated in Awake, then used whenever the tank dies
        private AudioSource m_ExplosionAudio;               // The audio source to play when the tank explodes
        private ParticleSystem m_ExplosionParticles;        // The particle system the will play when the tank is destroyed
        public Transform lastTank;
        public bool isPlayer;

        [SyncVar(hook = "OnChangeHealth")]
        public float currentHealth = m_StartingHealth;

        private void Awake()
        {
            // Instantiate the explosion prefab and get a reference to the particle system on it
            m_ExplosionParticles = Instantiate(m_ExplosionPrefab).GetComponent<ParticleSystem>();

            // Get a reference to the audio source on the instantiated prefab
            m_ExplosionAudio = m_ExplosionParticles.GetComponent<AudioSource>();

            // Disable the prefab so it can be activated when it's required
            m_ExplosionParticles.gameObject.SetActive(false);
        }

        private void Update()
        {
            // Only check health from server site
            if (isServer)
                SetHealthUI();
        }

        private void OnEnable()
        {
            // When the tank is enabled, reset the tank's health and whether or not it's dead
            currentHealth = m_StartingHealth;

            // Update the health slider's value and color
            SetHealthUI();
        }


        public void TakeDamage(float amount, Transform tank, Color color)
        {
            if (!isServer)
                return;

           // Debug.Log("Nombre1: " + tank.name);
          //  Debug.Log("Nombre2: " + this.transform.name);

            if(tank.name == this.transform.name)
                return;

            if (gameObject.tag == "NPC")
            {
                currentHealth -= amount;
                lastTank = tank;
                // Change the UI elements appropriately
                SetHealthUI();
                destroyTank(1); //NPC tank dies
            }

            else if(tank.tag == "RocketNPC" || tank.tag == "NPC")
            {
                currentHealth -= amount;
                lastTank = tank;
                // Change the UI elements appropriately
                SetHealthUI();
                destroyTank(2); //NPC kills player
            }

            else if (!gameManager.teamMatch || (gameManager.teamMatch &&
                                               !gameManager.GetTanksSameColors(tank, this.transform))){
                currentHealth -= amount;
                lastTank = tank;
                // Change the UI elements appropriately
                SetHealthUI();
                destroyTank(3);//Player kills another player
            }
        }



        public void destroyTank(int type)
        {
            if (currentHealth <= 0f)
            {
                //if(lastTank.tag != "RocketNPC" && lastTank.tag != "NPC") {
                if (type == 1) //Just NPCs kills add points to player
                {
                    gameManager.SetTankDeaths(lastTank);
                }
                if (gameObject.tag != "NPC")
                {
                    CmdOnDeath();
                    RpcRespawn();
                    RemoveShield();
                    currentHealth = m_StartingHealth;
                    OnChangeHealth(currentHealth, 3);
                }
                else
                {
                    RpcOnDeath();
                }
            }
        }

        [Server]
        [ClientRpc]
        private void RemoveShield()
        {
            Transform oldShield = gameObject.transform.Find("Shield");
            if (oldShield != null)
            {
                for (int i = 0; i < oldShield.childCount; i++)
                {
                    oldShield.GetChild(i).gameObject.SetActive(false);
                }
            }
        }

        private void OnChangeHealth(float oldHealth, float newHealth)
        {
            m_Slider.value = currentHealth;
            m_FillImage.color = Color.Lerp(m_ZeroHealthColor, m_FullHealthColor, currentHealth / m_StartingHealth);
        }

        [ClientRpc]
        private void RpcOnDeath()
        {
            m_ExplosionParticles.transform.position = transform.position;
            m_ExplosionParticles.gameObject.SetActive(true);
            m_ExplosionParticles.Play();
            m_ExplosionAudio.Play();
            gameObject.SetActive(false);
        }

        [TargetRpc]
        private void RpcRespawn()
        {
            if (isLocalPlayer)
            {
                GameObject[] sPoints = GameObject.FindGameObjectsWithTag("Spawn");
                //transform.position = sPoints[gameObject.GetComponent<TankMovement>().m_PlayerNumber - 1].transform.position;
                transform.position = sPoints[gameManager.GetRespawnPoint()].transform.position;
                gameObject.GetComponentInParent<Rigidbody>().velocity = new Vector3(0, 0, 0);
                m_ExplosionParticles.gameObject.SetActive(false);
            }
        }

        [Command]
        private void CmdOnDeath()
        {
            m_ExplosionParticles.transform.position = transform.position;
            m_ExplosionParticles.gameObject.SetActive(true);
            m_ExplosionParticles.Play();
            m_ExplosionAudio.Play();
            if (gameObject.tag == "NPC")
            {
                gameObject.SetActive(false);
            }
        }

        private void SetHealthUI()
        {
            // Set the slider's value appropriately
            m_Slider.value = currentHealth;

            // Interpolate the color of the bar between the choosen colours based on the current percentage of the starting health
            m_FillImage.color = Color.Lerp(m_ZeroHealthColor, m_FullHealthColor, currentHealth / m_StartingHealth);
        }
        /*
        [Command(requiresAuthority = false)]
        private void SendKiller()
        {
            gameManager.AddPlayerKill(lastTank);
        }
        */
    }
}