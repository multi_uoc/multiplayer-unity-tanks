using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using Complete;
using System;
using TMPro;
using System.Linq;

public class SyncTankManager : NetworkBehaviour
{
    private GameManager gameManager => GameManager.Instance;
    private PlayFabController playFab => PlayFabController.Instance;

    public static Action<Transform> OnLocalPlayerJoined;
    public static Action OnLocalPlayerReady;
    public TMP_Text playerNameText;
    public Transform rendererParent;

    [SyncVar(hook = nameof(OnChangeColor))]
    private Color playerColor;
    [SyncVar(hook = nameof(OnChangeName))]
    private string playerName;

    private void OnEnable()
    {
        PlayerUIManager.OnApplyPlayerPreferences += ChangeAppareance;
        PlayerUIManager.OnPlayerReady += PlayerReady;
    }

    private void OnDisable()
    {
        PlayerUIManager.OnApplyPlayerPreferences -= ChangeAppareance;
        PlayerUIManager.OnPlayerReady -= PlayerReady;
    }

    public override void OnStartLocalPlayer()
    {
        OnLocalPlayerJoined?.Invoke(this.transform);
        SetDisplayName();
    }

    public void PlayerReady(Transform player)
    {
        if (player == transform)
        {
            OnLocalPlayerReady?.Invoke();
            AddPlayer();
        }
    }

    private void AddPlayer()
    {
        gameManager.SetWaitingText();
        CmdAddPlayer();
    }

    [Command]
    private void CmdAddPlayer()
    {
        gameManager.AddPlayerTank(this.gameObject, playerColor, playerNameText.text);
    }

    private void ChangeAppareance(Transform player, Color newColor)
    {
        if (!NetworkClient.active)
        {
            return;
        }

        if (player == this.transform)
        {
            CmdChangeAppareance(newColor);
        }
    }

    private void SetDisplayName()
    {
        if (!NetworkClient.active)
        {
            return;
        }

        string displayName;

        if (playFab != null)
        {
            displayName = playFab.displayName;
        }
        else if (PlayerPrefs.HasKey("LAN_NAME"))
        {
            displayName = PlayerPrefs.GetString("LAN_NAME");
        }
        else
        {
            displayName = "Player";
        }

        CmdSetDisplayName(displayName);
    }

    [Command]
    void CmdChangeAppareance(Color newColor)
    {
        if (newColor != Color.clear)
        {
            playerColor = newColor;
            this.transform.GetComponent<TankShooting>().SetColor(playerColor);
        }
        gameManager.UpdatePlayerTankInfo(this.gameObject, playerColor, "");
    }

    [Command]
    void CmdSetDisplayName(string displayName)
    {
        playerName = displayName;
        gameManager.UpdatePlayerTankInfo(this.gameObject, Color.clear, playerName);
    }

    private void OnChangeColor(Color oldValue, Color newValue)
    {
        foreach (MeshRenderer child in rendererParent.GetComponentsInChildren<MeshRenderer>())
        {
            child.material.color = newValue;
        }
    }

    private void OnChangeName(string oldValue, string newValue)
    {
        playerNameText.SetText(newValue);
    }
}
