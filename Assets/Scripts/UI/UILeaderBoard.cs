﻿using UnityEngine;
using System.Collections;
using TMPro;
using PlayFab.ClientModels;
using PlayFab;

public class UILeaderBoard : MonoBehaviour
{
    public TMPro.TextMeshProUGUI leaderboardRank;
    public TMPro.TextMeshProUGUI leaderboardName;
    public TMPro.TextMeshProUGUI leaderboardScore;

    //Get the players with the top 10 high scores in the game
    public void RequestLeaderboard()
    {
        if (leaderboardRank.text == "")
        {
            PlayFabClientAPI.GetLeaderboard(new GetLeaderboardRequest
            {
                StatisticName = "Tanks Leaderboard",
                StartPosition = 0,
                MaxResultsCount = 10
            }, result => DisplayLeaderboard(result), FailureCallback);
            LobbyUIManager.instance.ShowLeaderBoardPanel();
        }
    }

    private void DisplayLeaderboard(GetLeaderboardResult result)
    {
        for (int i = 0; i < result.Leaderboard.Count; i++)
        {
            var playerData = result.Leaderboard[i];
            string playerName = playerData.DisplayName;
            int playerScore = playerData.StatValue;

            leaderboardRank.text += $"# {i + 1}\n";
            leaderboardName.text += $"{playerName}\n";
            leaderboardScore.text += $"{playerScore}\n";
        }
    }

    private void FailureCallback(PlayFabError error)
    {
        Debug.LogWarning("Something went wrong with your API call. Here's some debug information:");
        Debug.LogError(error.GenerateErrorReport());
    }
}
