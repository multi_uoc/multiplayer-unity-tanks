using Complete;
using Mirror;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUIManager : MonoBehaviour
{
    public delegate void PlayerOptions(Transform player, Color newColor);
    public static PlayerOptions OnApplyPlayerPreferences;
    public static event Action<Transform> OnPlayerReady;

    [SerializeField] private TMP_InputField inputText;
    [SerializeField] private GameObject readyButton;
    [SerializeField] private GameObject customizeButton;

    private Color pickedColor = Color.clear;
    private Transform playerTransform;
    private bool panelShown = false;
    private bool initialSelection = true;
    private Vector3 shownPos = new Vector3(401.5601f, -161.964f, 0);
    private Vector3 hiddenPos = new Vector3(401.5601f, -457.4942f, 0);
    private GameObject buttonRed;
    private GameObject buttonBlue;
    private GameObject buttonGreen;
    private GameObject buttonWhite;
    private GameObject buttonCyan;
    private GameObject buttonMagenta;

    private void Start()
    {
        buttonRed = GameObject.Find("Red");
        buttonBlue = GameObject.Find("Blue");
        buttonGreen = GameObject.Find("Green");
        buttonWhite = GameObject.Find("White");
        buttonCyan = GameObject.Find("Cyan");
        buttonMagenta = GameObject.Find("Magenta");
        TogglePanel();
    }

    private void OnEnable()
    {
        SyncTankManager.OnLocalPlayerJoined += GetPlayerTransform;
        GameManager.OnStartGame += DeactivateToggle;
    }

    private void OnDisable()
    {
        SyncTankManager.OnLocalPlayerJoined -= GetPlayerTransform;
        GameManager.OnStartGame -= DeactivateToggle;
    }

    private void GetPlayerTransform(Transform player)
    {
        playerTransform = player;

        customizeButton.SetActive(true);
    }

    public void SetPlayerColor(string color)
    {
        Color newColor = Color.clear;
        DeactivateOutline();
        switch (color)
        {
            case "Red":
                newColor = Color.red;
                buttonRed.GetComponent<Outline>().enabled = true;
                break;
            case "Blue":
                newColor = Color.blue;
                buttonBlue.GetComponent<Outline>().enabled = true;
                break;
            case "Green":
                newColor = Color.green;
                buttonGreen.GetComponent<Outline>().enabled = true;
                break;
            case "White":
                newColor = Color.white;
                buttonWhite.GetComponent<Outline>().enabled = true;
                break;
            case "Cyan":
                newColor = Color.cyan;
                buttonCyan.GetComponent<Outline>().enabled = true;
                break;
            case "Magenta":
                newColor = Color.magenta;
                buttonMagenta.GetComponent<Outline>().enabled = true;
                break;
        }
        pickedColor = newColor;
    }

    private void DeactivateOutline()
    {
        buttonRed.GetComponent<Outline>().enabled = false;
        buttonBlue.GetComponent<Outline>().enabled = false;
        /*        buttonGreen.GetComponent<Outline>().enabled = false;
                buttonWhite.GetComponent<Outline>().enabled = false;
                buttonCyan.GetComponent<Outline>().enabled = false;
                buttonMagenta.GetComponent<Outline>().enabled = false;*/
    }

    public void ApplyPlayerPreferences()
    {
        // It's mandatory to pick a color and a nickname thte first time
        if (!initialSelection || pickedColor != Color.clear)
        {
            OnApplyPlayerPreferences?.Invoke(playerTransform, pickedColor);
            TogglePanel();

            if (initialSelection)
            {
                initialSelection = false;
                readyButton.SetActive(true);
            }
        }
    }

    public void TogglePanel()
    {
        transform.localPosition = panelShown ? hiddenPos : shownPos;
        panelShown = !panelShown;
    }

    public void PlayerReady()
    {
        OnPlayerReady?.Invoke(playerTransform);
        readyButton.SetActive(false);
    }

    public void DeactivateToggle()
    {
        customizeButton.SetActive(false);
    }
}
