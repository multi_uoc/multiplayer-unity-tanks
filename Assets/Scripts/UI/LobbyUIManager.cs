using Complete;
using Mirror;
using Mirror.Discovery;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LobbyUIManager : MonoBehaviour
{
    private PlayFabController playFab => PlayFabController.Instance;

    public GameObject GamePanel;
    public List<GameObject> gameButtons = new List<GameObject>();
    public GameObject MenuPanel;
    public ServerButton ServerButton;
    private MatchmakingManager matchmakingManager;
    private int getConnectionType = 0;

    [Header("Panels")]
    [SerializeField] private GameObject controlsPanel;
    [SerializeField] private GameObject friendsPanel;
    [SerializeField] private GameObject leaderBoardPanel;

    [Header("DisplayName")]
    [SerializeField] private GameObject displayNamePanel;
    [SerializeField] private GameObject editPanel;
    [SerializeField] private TMP_Text displayName;
    [SerializeField] private TMP_InputField editField;
    [SerializeField] private Sprite editSprite;
    [SerializeField] private Sprite checkSprite;
    [SerializeField] private Image editModeImage;
    [SerializeField] private GameObject checkTickImage;
    [SerializeField] private TMP_Text errorMessage;
    [SerializeField] private TMP_Text errorFriendMessage;
    [SerializeField] private TMP_Text errorLeaderBoardMessage;
    [SerializeField] private TMP_Text leaderBoardRank;
    [SerializeField] private TMP_Text leaderBoardPlayer;
    [SerializeField] private TMP_Text leaderBoardScore;

    [Header("Game Status")]
    [SerializeField] private TMP_Text displayStatus;

    private bool isEditing = false;
    private string newDisplayName;

    private bool isClientConnecting = false;

    public static LobbyUIManager instance;

    private void OnEnable()
    {
        ServerButton.OnServerClick += Connect;
        GameManager.OnPlayerStopGame += StopClient;
        GameManager.OnServerStopGame += StopHost;
        SceneManager.sceneLoaded += OnSceneLoaded;
        PlayFabController.OnDisplayNameUpdated += RefreshDisplayName;
        PlayFabController.OnError += SetErrorText;
    }

    private void OnDisable()
    {
        ServerButton.OnServerClick -= Connect;
        GameManager.OnPlayerStopGame -= StopClient;
        GameManager.OnServerStopGame -= StopHost;
        SceneManager.sceneLoaded -= OnSceneLoaded;
        PlayFabController.OnDisplayNameUpdated -= RefreshDisplayName;
    }

    private void Start()
    {
        ActivateDisplayNamePanel(true);
        matchmakingManager = GameObject.Find("MatchmakingManager").GetComponent<MatchmakingManager>();
        instance = this;
    }

    public string getPlayerIP()
    {
        var url = "https://api.ipify.org/";

        WebRequest request = WebRequest.Create(url);
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        Stream dataStream = response.GetResponseStream();
        using StreamReader reader = new StreamReader(dataStream);
        var ip = reader.ReadToEnd();
        reader.Close();

        return ip;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == SceneNames.ONLINE_LOBBY)
        {
            SetDisplayStatus("");
            ActivateDisplayNamePanel(true);
        }
        else if (scene.name == SceneNames.ONLINE_GAME)
        {
            SetDisplayStatus("");
            ActivateDisplayNamePanel(false);
        }
    }

    private void ToggleGamePanel(int index)
    {
        GamePanel.SetActive(true);
        MenuPanel.SetActive(false);
        for (int i = 0; i < gameButtons.Count; i++)
        {
            gameButtons[i].SetActive(i == index ? true : false);
        }
    }

    public void ToggleEditPanel()
    {
        bool nextSate = !isEditing;

        if (nextSate)
        {
            displayName.gameObject.SetActive(false);
            editPanel.SetActive(true);
            isEditing = true;
            editModeImage.sprite = checkSprite;
            checkTickImage.SetActive(true);
        }
        else
        {
            if (!string.IsNullOrEmpty(newDisplayName) && newDisplayName.Length >= 3)
            {
                playFab.SetUserDisplayName(newDisplayName);
                editPanel.SetActive(false);
                editField.text = "";
                isEditing = false;
                editModeImage.sprite = editSprite;
                checkTickImage.SetActive(false);
                SetErrorText("");
            }
            else
            {
                SetErrorText("Username must contain between 3 and 20 characters");
            }
        }
    }

    public void SetDisplayName(string value)
    {
        newDisplayName = value;
    }

    public void CancelEdit()
    {
        isEditing = false;
        editModeImage.sprite = editSprite;
        checkTickImage.SetActive(false);
        editField.text = "";
        displayName.gameObject.SetActive(true);
        editPanel.SetActive(false);
        SetErrorText("");
    }

    public void SetDisplayStatus(string message)
    {
        displayStatus.text = message;
        if (SceneManager.GetActiveScene().name == "GameScene")
        {
            displayStatus.rectTransform.position = new Vector3(-238, -67, 0);
        }
    }

    private void RefreshDisplayName(bool updated)
    {
        if (updated)
        {
            ActivateDisplayNamePanel(true);
        }
        else
        {
            displayName.gameObject.SetActive(true);
        }
    }

    private void ActivateDisplayNamePanel(bool active)
    {
        displayNamePanel.gameObject.SetActive(active);
        if (active)
        {
            displayName.gameObject.SetActive(true);
            displayName.text = "Welcome back, " + playFab.displayName;
        }
    }

    private void ToggleDiscoveryPanels()
    {
        GamePanel.SetActive(false);
        MenuPanel.SetActive(true);
        for (int i = 0; i < gameButtons.Count; i++)
        {
            gameButtons[i].SetActive(false);
        }
    }

    public int CheckConnectionType()
    {
        //0 Client, 1 Host, 2 Server
        return getConnectionType;
    }

    public bool CheckClientIsConnecting()
    {
        return isClientConnecting;
    }

    public void PrepareConnectionToServer()
    {
        getConnectionType = 0;
        SetDisplayStatus("Looking for players");
        matchmakingManager.MatchmakingCreateTicket();
    }

    public void StartHost()
    {
        getConnectionType = 1;
        matchmakingManager.MatchmakingCreateTicket();
        NetworkManager.singleton.StartHost();
        ActivateDisplayNamePanel(false);
        ToggleGamePanel(1);
    }

    public void StartServer()
    {
        getConnectionType = 2;
        matchmakingManager.MatchmakingCreateTicket();
        NetworkManager.singleton.StartServer();
        ActivateDisplayNamePanel(false);
        ToggleGamePanel(2);
    }

    public void Connect(ServerResponse info)
    {
        NetworkManager.singleton.StartClient(info.uri);
        ToggleGamePanel(0);
    }

    public void DirectConnection(string ip)
    {
        isClientConnecting = true;
        NetworkManager.singleton.StartClient(new Uri("kcp://" + ip + ":7777"));
        ToggleGamePanel(0);
    }

    public void StopHost()
    {
        matchmakingManager.CancelMatchmakingTicket();
        NetworkManager.singleton.StopHost();
        ToggleDiscoveryPanels();
        GameObject.Destroy(GameObject.Find("MatchmakingManager"));
    }

    public void StopServer()
    {
        matchmakingManager.CancelMatchmakingTicket();
        NetworkManager.singleton.StopServer();
        ToggleDiscoveryPanels();
        GameObject.Destroy(GameObject.Find("MatchmakingManager"));
    }

    public void StopClient()
    {
        isClientConnecting = false;
        matchmakingManager.CancelMatchmakingTicket();
        NetworkManager.singleton.StopClient();
        ToggleDiscoveryPanels();
        GameObject.Destroy(GameObject.Find("MatchmakingManager"));
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ShowControlsPanel()
    {
        controlsPanel.SetActive(true);
    }

    public void DimissControlsPanel()
    {
        controlsPanel.SetActive(false);
    }
    public void ShowFriendsPanel()
    {
        friendsPanel.SetActive(true);
    }

    public void DimissFriendsPanel()
    {
        friendsPanel.SetActive(false);
        errorFriendMessage.text = "";
    }
    public void ShowLeaderBoardPanel()
    {
        leaderBoardPanel.SetActive(true);
    }

    public void DimissLeaderBoardPanel()
    {
        leaderBoardPanel.SetActive(false);
        leaderBoardRank.text = "";
        leaderBoardPlayer.text = "";
        leaderBoardScore.text = "";
    }
    public void LogOut()
    {
        playFab.LogOut();
    }

    private void SetErrorText(string error)
    {
        errorMessage.text = error;
    }
}
