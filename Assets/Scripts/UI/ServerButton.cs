﻿using Mirror.Discovery;
using System;
using UnityEngine;
using UnityEngine.UI;

public class ServerButton : MonoBehaviour
{
    public static Action<ServerResponse> OnServerClick;
    public Text text;
    [SerializeField] private ServerResponse serverInfo;

    public void SetData(ServerResponse info)
    {
        serverInfo = info;
        text.text = serverInfo.EndPoint.Address.ToString();
    }

    public void ClickServer()
    {
        OnServerClick?.Invoke(serverInfo);
    }
}
