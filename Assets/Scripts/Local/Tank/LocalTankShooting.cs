using Complete;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class LocalTankShooting : MonoBehaviour
{
    public int m_PlayerNumber = 1;              // Used to identify the different players.
    public GameObject m_Shell;
    public GameObject m_ShellAlt;               // Prefab of the shell alt
    public GameObject m_ShellLaser; 
    public Transform m_FireTransform;           // A child of the tank where the shells are spawned.
    public Transform m_LaserTransform;
    public Slider m_AimSlider;                  // A child of the tank that displays the current launch force.
    public AudioSource m_ShootingAudio;         // Reference to the audio source used to play the shooting audio. NB: different to the movement audio source.
    public AudioClip m_ChargingClip;            // Audio that plays when each shot is charging up.
    public AudioClip m_FireClip;                // Audio that plays when each shot is fired.
    public float m_MinLaunchForce = 15f;        // The force given to the shell if the fire button is not held.
    public float m_MaxLaunchForce = 30f;        // The force given to the shell if the fire button is held for the max charge time.
    public float m_MaxChargeTime = 0.75f;       // How long the shell can charge for before it is fired at max force.
    public PlayerInput m_TankInput;

    private string m_FireButton;                // The input axis that is used for launching shells.
    private float m_CurrentLaunchForce;         // The force that will be given to the shell when the fire button is released.
    private float m_ChargeSpeed;                // How fast the launch force increases, based on the max charge time.

    private bool isDisabled = false;            // To avoid enabling / disabling Input System when tank is destroyed
    private bool isLaserReady = true;
    private float timeToReload;

    public Color color;

    public bool HasLaser { get; set; } = false;
    public bool HasAltFire { get; set; } = false;


    private void OnEnable()
    {
        // When the tank is turned on, reset the launch force and the UI
        m_CurrentLaunchForce = m_MinLaunchForce;
        m_AimSlider.value = m_MinLaunchForce;
        isDisabled = false;
    }

    private void OnDisable()
    {
        isDisabled = true;
    }

    private void Start()
    {
        m_TankInput.actions["Fire"].performed += ctx => OnFire(ctx);
        m_TankInput.actions["FireAlt"].performed += ctx => OnFireAlt(ctx);
        m_TankInput.actions["Laser"].performed += ctx => OnLaser(ctx);

        // The rate that the launch force charges up is the range of possible forces by the max charge time.
        m_ChargeSpeed = (m_MaxLaunchForce - m_MinLaunchForce) / m_MaxChargeTime;
    }

    private void OnFire(InputAction.CallbackContext obj)
    {
        if (!isDisabled)
        {
            // When the value read is higher than the default Button Press Point, the key has been pressed
            if (obj.ReadValue<float>() >= InputSystem.settings.defaultButtonPressPoint)
            {
                FireProjectile(0);
            }
        }
    }

    private void OnFireAlt(InputAction.CallbackContext obj)
    {
        if (!HasAltFire)
        {
            return;
        }
        if (!isDisabled)
        {
            // When the value read is higher than the default Button Press Point, the key has been pressed
            if (obj.ReadValue<float>() >= InputSystem.settings.defaultButtonPressPoint)
            {
                FireProjectile(1);
            }
        }
    }

    private void OnLaser(InputAction.CallbackContext obj)
    {
        if (!HasLaser)
        {
            return;
        }
        if (!isDisabled && isLaserReady)
        {
            // When the value read is higher than the default Button Press Point, the key has been pressed
            if (obj.ReadValue<float>() >= InputSystem.settings.defaultButtonPressPoint)
            {
                isLaserReady = false;
                timeToReload = 0;
                FireProjectile(3);
            }
        }
    }


    private void FireProjectile(int shootType)
    {
        m_ShootingAudio.clip = m_FireClip;
        m_ShootingAudio.Play();

        // Reset the launch force.  This is a precaution in case of missing button events
        m_CurrentLaunchForce = m_MinLaunchForce;

        GameObject bullet;
        if (shootType == 0)
        {
            //Regular Shell
            bullet = Instantiate(m_Shell, m_FireTransform.position, m_FireTransform.rotation);
            bullet.GetComponent<ShellExplosion>().Setup(m_MinLaunchForce, transform, color);
        }
        else if (shootType == 1)
        {
            //Alt shell
            bullet = Instantiate(m_ShellAlt, m_FireTransform.position, m_FireTransform.rotation);
            bullet.GetComponent<ShellExplosion>().Setup(m_MinLaunchForce * 1.50f, transform, color);
        }
        else
        {
            //Laser Beam
            bullet = Instantiate(m_ShellLaser, m_LaserTransform.position, m_LaserTransform.rotation);
            bullet.GetComponent<ShellExplosion>().Setup(m_MinLaunchForce * 3f, transform, color);
        }

        Transform shield = transform.Find("Shield");
        if (shield != null)
        {
            foreach (Collider collider in shield.GetComponentsInChildren<Collider>())
            {
                Physics.IgnoreCollision(bullet.GetComponent<Collider>(), collider, true);
            }
        }
    }
}
