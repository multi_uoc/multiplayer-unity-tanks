using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SceneNames
{
    public static readonly string MODE_SELECTION = "ModeSelection";
    public static readonly string LOGIN = "Login";
    public static readonly string ONLINE_LOBBY = "Lobby";
    public static readonly string ONLINE_GAME = "GameScene";

    public static readonly string LAN_LOBBY = "LANLobby";

    public static readonly string LOCAL_GAME = "LocalGame";
}
