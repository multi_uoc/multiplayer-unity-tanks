using System;
using UnityEngine;

public sealed class Shield : MonoBehaviour
{
    public event Action<Shield> HitReceived;
    private void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
        HitReceived?.Invoke(this);
    }
}
