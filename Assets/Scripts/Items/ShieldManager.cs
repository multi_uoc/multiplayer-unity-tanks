using UnityEngine;

public sealed class ShieldManager : MonoBehaviour
{
    [SerializeField]
    private float m_Speed = 0.5f;

    private Shield[] m_Shields;

    private void Start()
    {
        m_Shields = GetComponentsInChildren<Shield>(true);
        foreach (Shield shield in m_Shields)
        {
            shield.HitReceived += OnHitReceived;
        }
    }

    private void Update()
    {
        transform.Rotate(new Vector3(0f, m_Speed, 0f));
    }

    private void OnHitReceived(Shield obj)
    {
        obj.gameObject.SetActive(false);
    }
}
