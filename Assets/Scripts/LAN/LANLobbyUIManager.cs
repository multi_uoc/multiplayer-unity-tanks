using Complete;
using Mirror;
using Mirror.Discovery;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LANLobbyUIManager : MonoBehaviour
{
    public NetworkDiscovery networkDiscovery;
    public GameObject GamePanel;
    public List<GameObject> gameButtons = new List<GameObject>();
    public GameObject MenuPanel;
    public GameObject ServerPanel;
    public ServerButton ServerButton;
    private Dictionary<long, ServerResponse> discoveredServers = new Dictionary<long, ServerResponse>();

    [Header("Panels")]
    [SerializeField] private GameObject controlsPanel;

    [Header("DisplayName")]
    [SerializeField] private GameObject displayNamePanel;
    [SerializeField] private GameObject editPanel;
    [SerializeField] private TMP_Text displayName;
    [SerializeField] private TMP_InputField editField;
    [SerializeField] private Sprite editSprite;
    [SerializeField] private Sprite checkSprite;
    [SerializeField] private Image editModeImage;
    [SerializeField] private GameObject checkTickImage;

    private bool isEditing = false;
    private string newDisplayName;

    private void OnEnable()
    {
        ServerButton.OnServerClick += Connect;
        GameManager.OnPlayerStopGame += StopClient;
        GameManager.OnServerStopGame += StopHost;
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        ServerButton.OnServerClick -= Connect;
        GameManager.OnPlayerStopGame -= StopClient;
        GameManager.OnServerStopGame -= StopHost;
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void Start()
    {
        ActivateDisplayNamePanel(true);
    }

    public void OnServerDiscovered(ServerResponse info)
    {
        if (!discoveredServers.ContainsKey(info.serverId))
        {
            discoveredServers[info.serverId] = info;
            CreateServerButton(info);
        }
    }

    private void CreateServerButton(ServerResponse info)
    {
        Instantiate(ServerButton, ServerPanel.transform).SetData(info);
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == SceneNames.LAN_LOBBY)
        {
            ActivateDisplayNamePanel(true);
        }
        else if (scene.name == SceneNames.ONLINE_GAME)
        {
            ActivateDisplayNamePanel(false);
        }
    }

    private void ToggleGamePanel(int index)
    {
        GamePanel.SetActive(true);
        MenuPanel.SetActive(false);
        ServerPanel.SetActive(false);
        for (int i = 0; i < gameButtons.Count; i++)
        {
            gameButtons[i].SetActive(i == index);
        }
    }

    public void ToggleEditPanel()
    {
        bool nextSate = !isEditing;

        if (nextSate)
        {
            displayName.gameObject.SetActive(false);
            editPanel.SetActive(true);
            isEditing = true;
            editModeImage.sprite = checkSprite;
            checkTickImage.SetActive(true);
        }
        else if (!string.IsNullOrEmpty(newDisplayName) && newDisplayName.Length >= 3)
        {
            PlayerPrefs.SetString("LAN_NAME", newDisplayName);
            editPanel.SetActive(false);
            editField.text = "";
            isEditing = false;
            editModeImage.sprite = editSprite;
            checkTickImage.SetActive(false);

            ActivateDisplayNamePanel(true);
        }
    }

    public void SetDisplayName(string value)
    {
        newDisplayName = value;
    }

    public void CancelEdit()
    {
        isEditing = false;
        editModeImage.sprite = editSprite;
        checkTickImage.SetActive(false);
        editField.text = "";
        displayName.gameObject.SetActive(true);
        editPanel.SetActive(false);
    }

    private void ActivateDisplayNamePanel(bool active)
    {
        displayNamePanel.gameObject.SetActive(active);
        if (active)
        {
            displayName.gameObject.SetActive(true);

            if (PlayerPrefs.HasKey("LAN_NAME"))
            {
                displayName.text = "Welcome back, " + PlayerPrefs.GetString("LAN_NAME");
            }
            else
            {
                displayName.text = "Welcome, set a display name";
            }
        }
    }

    private void ToggleDiscoveryPanels()
    {
        GamePanel.SetActive(false);
        MenuPanel.SetActive(true);
        ServerPanel.SetActive(true);
        for (int i = 0; i < gameButtons.Count; i++)
        {
            gameButtons[i].SetActive(false);
        }
    }

    public void FindServer()
    {
        discoveredServers.Clear();
        networkDiscovery.StartDiscovery();
    }

    public void StartHost()
    {
        discoveredServers.Clear();
        NetworkManager.singleton.StartHost();
        networkDiscovery.AdvertiseServer();
        ToggleGamePanel(1);
    }

    public void SartServer()
    {
        discoveredServers.Clear();
        NetworkManager.singleton.StartServer();
        networkDiscovery.AdvertiseServer();
        ToggleGamePanel(2);
    }

    public void Connect(ServerResponse info)
    {
        networkDiscovery.StopDiscovery();
        NetworkManager.singleton.StartClient(info.uri);
        ToggleGamePanel(0);
    }

    public void StopHost()
    {
        NetworkManager.singleton.StopHost();
        networkDiscovery.StopDiscovery();
        ToggleDiscoveryPanels();
    }

    public void StopServer()
    {
        NetworkManager.singleton.StopServer();
        networkDiscovery.StopDiscovery();
        ToggleDiscoveryPanels();
    }

    public void StopClient()
    {
        NetworkManager.singleton.StopClient();
        networkDiscovery.StopDiscovery();
        ToggleDiscoveryPanels();
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ShowControlsPanel()
    {
        controlsPanel.SetActive(true);
    }

    public void DimissControlsPanel()
    {
        controlsPanel.SetActive(false);
    }
}
